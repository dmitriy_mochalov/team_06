package ru.edu.project.backend.stub.students;

//import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class InMemoryStubStudentService implements StudentService {
    /**
     * Локальное хранилище данных в RAM.
     */
    private Map<Long, StudentInfo> db = new ConcurrentHashMap<>();

    /**
     * Локальный счетчик записей.
     */
    private AtomicLong idCount = new AtomicLong(0);

    /**
     * info.
     */
    private StudentInfo info = StudentInfo.builder()
            .id(12l)
            .groupId(422l)
            .firstName("anton")
            .secondName("antonovich")
            .lastName("antonov")
            .build();


//    /**
//     * Добавление данных студента.
//     *
//     * @param studentForm
//     * @return StudentInfo
//     */
//    @Override
//    public StudentInfo editStudent(final StudentForm studentForm) {
//        return info;
//    }

    /**
     * Просмотр информации о студенте.
     *
     * @param recordId
     * @return StudentInfo
     */
    @Override
    public StudentInfo getDetailedInfo(final Long recordId) {
        return info;
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    public List<StudentInfo> getAllStudents() {
        return new ArrayList<StudentInfo>(db.values());
    }
}
