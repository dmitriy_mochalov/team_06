package ru.edu.project.backend.api.students;

import java.util.List;

public interface StudentService {

//    /**
//     * Добавление/редактирование данных студента.
//     * @param studentForm
//     * @return StudentInfo
//     */
//    StudentInfo editStudent(StudentForm studentForm);

    /**
     * Просмотр информации о студенте.
     * @param recordId
     * @return StudentInfo
     */
    StudentInfo getDetailedInfo(Long recordId);

    /**
     * Вывод всех студентов.
     * @return List
     */
    List<StudentInfo> getAllStudents();
}

