package ru.edu.project.backend.api.students;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.util.HashMap;

@Getter
@Setter
@Builder
@Jacksonized
public class StudentInfo {
    /**
     * id студента.
     */
    private Long id;

    /**
     * id группы студента.
     */
    private Long groupId;

    /**
     * Имя студента.
     */
    private String firstName;

    /**
     * Отчество студента.
     */
    private String secondName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * Ведомость успеваемости студента.
     */
    private HashMap<Integer, String> reportCard;

}
