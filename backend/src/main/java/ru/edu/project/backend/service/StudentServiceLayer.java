package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.da.StudentDALayer;
import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("StudentServiceLayer")
public class StudentServiceLayer implements StudentService {

    /**
     * Зависимость для слоя доступа к данным услуг.
     */
    @Autowired
    private StudentDALayer daLayer;

    /**
     * Просмотр информации о студенте.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    public StudentInfo getDetailedInfo(final Long id) {
        return daLayer.getById(id);
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    public List<StudentInfo> getAllStudents() {
        return null;
    }
}
