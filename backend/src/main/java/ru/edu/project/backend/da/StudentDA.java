package ru.edu.project.backend.da;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.api.students.StudentInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Data Access слой для job_table.
 */
@Component
@Profile("JDBC_TEMPLATE")
public class StudentDA implements StudentDALayer {

    /**
     * Запрос для поиска всех активированных записей по списку id.
     */
    public static final String QUERY_AVAILABLE_BY_ID = "SELECT * FROM STUDENTS WHERE id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Получаем услугу по id.
     *
     * @param id
     * @return job
     */
    @Override
    public StudentInfo getById(final Long id) {
        return jdbcTemplate.query(QUERY_AVAILABLE_BY_ID, this::singleRowMapper, id);
    }

    @SneakyThrows
    private StudentInfo singleRowMapper(final ResultSet resultSet) {
        resultSet.next();
        return mapRow(resultSet);
    }

    private StudentInfo mapRow(final ResultSet resultSet) throws SQLException {
        return StudentInfo.builder()
                .id(resultSet.getLong("student_id"))
                .firstName(resultSet.getString("first_name"))
                .secondName(resultSet.getString("second_name"))
                .lastName(resultSet.getString("last_name"))
                .groupId(resultSet.getLong("group_id"))
                .build();
    }

}
