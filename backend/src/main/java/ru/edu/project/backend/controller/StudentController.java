package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController implements StudentService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private StudentService delegate;

    /**
     * Просмотр информации о студенте.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    @GetMapping("/getStudentInfo/{id}")
    public StudentInfo getDetailedInfo(@PathVariable("id") final Long id) {
        return delegate.getDetailedInfo(id);
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    @GetMapping("/getAvailable")
    public List<StudentInfo> getAllStudents() {
        return null;
    }
}
