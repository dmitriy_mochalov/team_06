package ru.edu.project.backend.da;
import ru.edu.project.backend.api.students.StudentInfo;

public interface StudentDALayer {

    /**
     * Получение student info по id.
     *
     * @param id
     * @return job
     */
    StudentInfo getById(Long id);

}
