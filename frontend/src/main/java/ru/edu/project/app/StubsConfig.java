package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.stub.students.InMemoryStubStudentService;

@Configuration
@Profile("STUBS")
public class StubsConfig {

    /**
     * Заглушка сервиса.
     * @return bean
     */
    @Bean
    public StudentService studentServiceBean() {
        return new InMemoryStubStudentService();
    }
}
