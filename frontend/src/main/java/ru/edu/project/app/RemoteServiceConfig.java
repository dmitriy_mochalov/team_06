package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.RestServiceInvocationHandler;
import ru.edu.project.backend.api.students.StudentService;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси для RequestService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public StudentService requestServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/request");
        return getProxy(handler, StudentService.class);
    }

    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }

}
