package ru.edu.project.frontend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;

@RequestMapping("/")
@Controller
public class FrontController {
    /**
     * logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontController.class);

    /**
     * Ссылка на сервис студента.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Точка входа.
     *
     * @return view
     */
    @GetMapping("/")
    public String index() {
        return "index";
    }

    /**
     * Student authentication/ get id.
     *
     * @return view
     */
    @GetMapping("/student/")
    public String studentIndex() {
        return "student";
    }

    /**
     * Get student info by id.
     *
     * @param id
     * @return modelAndView
     */
    @GetMapping("/student_info/")
    public ModelAndView info(final @RequestParam(value = "id") Long id) {
        ModelAndView model = new ModelAndView("info");
        StudentInfo info = studentService.getDetailedInfo(id);
        LOGGER.info(studentService.getClass().getSimpleName());
        LOGGER.info(info.toString());
        if (info == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }
        model.addObject("record", info);
        return model;
    }
}
